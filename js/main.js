$(document).ready(function() {

    
    //sliders 
    $('.otziv').owlCarousel({
            center:false,
            loop:true,
            margin: 50,
            stagePadding: 0,
            //animateIn: 'zoomIn',
            //animateOut: 'slideOutLeft',
            mouseDrag:true,
            thumbs: true,
            thumbsPrerendered: true,
            touchDrag:true,
            autoplay:false,
            autoplayTimeout:4000,
            smartSpeed:1000, 
            autoplayHoverPause: false,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            },
        });

    var sliders = function() {

        $('.pic1').owlCarousel({
            center:false,
            loop:true,
            margin: 50,
            stagePadding: 0,
            //animateIn: 'zoomIn',
            //animateOut: 'slideOutLeft',
            mouseDrag:true,
            thumbs: true,
            thumbsPrerendered: true,
            touchDrag:true,
            autoplay:true,
            autoplayTimeout:4000,
            smartSpeed:1000, 
            autoplayHoverPause: false,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            },
        });

        $('.pic2').owlCarousel({
            center:false,
            loop:true,
            margin: 50,
            stagePadding: 0,
            //animateIn: 'zoomIn',
            //animateOut: 'slideOutLeft',
            mouseDrag:true,
            thumbs: true,
            thumbsPrerendered: true,
            touchDrag:true,
            autoplay:true,
            autoplayTimeout:4000,
            smartSpeed:1000, 
            autoplayHoverPause: false,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            },
        });

        $('.pic3').owlCarousel({
            center:false,
            loop:true,
            margin: 50,
            stagePadding: 0,
            //animateIn: 'zoomIn',
            //animateOut: 'slideOutLeft',
            mouseDrag:true,
            thumbs: true,
            thumbsPrerendered: true,
            touchDrag:true,
            autoplay:true,
            autoplayTimeout:4000,
            smartSpeed:1000, 
            autoplayHoverPause: false,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            },
        });

    }
    sliders();

    $('a[href^="#"]').click(function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1000);
    });

    //mask input
    $('.phone').mask('+7 (999) 999 99 99');   

    
    // tabs
    $('.tabs_menu li:first-child').addClass('active');
    $(".tabs_menu li").click(function() { 
        
        setTimeout(function(){
            sliders();
        }, 10);
        if (!$(this).hasClass("active")) { 
            var i = $(this).index(); 
            $(".tabs_menu li.active").removeClass("active"); 
            $(".tabs .active").hide().removeClass("active"); 
            $(this).addClass("active"); 
            $($(".tabs").children(".info")[i]).fadeIn(500).addClass("active"); 
        }
        
    });


    //responsive js 
    if($(window).width() < 1024){

        $(window).scroll(function(){
            var scrolltop = $(window).scrollTop();
        });

        $('.his1').insertAfter('.his2');    

    }
    if($(window).width() > 1024){
    }



    new WOW(
        {
            offset: 50,
        }
    ).init();


});


